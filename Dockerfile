FROM python:3.11

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

RUN apt-get update && apt-get install -y \
    libpq-dev \
    gcc

COPY poetry.lock pyproject.toml ./

ENV LANG en_US.UTF-8
ENV POETRY_VIRTUALENVS_CREATE=0

RUN pip install --upgrade pip && pip install poetry
RUN poetry install -n --no-ansi --with dev

COPY . /app/

EXPOSE 8000

CMD ["poetry", "run", "gunicorn", "--bind", "0.0.0.0:8000", "weather_app.wsgi:application"]
