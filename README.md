# Django Weather Project

Welcome to the Django weather project! This is a basic project for getting weather from api with running in Docker.

## Prerequisites

Before you get started, make sure you have the following prerequisites installed on your system:

- [Docker](https://www.docker.com/get-started)
- [Docker Compose](https://docs.docker.com/compose/install/)
- Git (optional, for cloning the repository)

## Getting Started

1. Clone this repository to your local machine (if you haven't already):

   ```bash
   git clone https://gitlab.com/llpvqll/django_weather_app.git
   cd weather_app
   chmod +x entrypoint.sh
   docker compose up --build
