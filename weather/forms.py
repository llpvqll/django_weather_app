from django import forms
from .models import WeatherData


class WeatherDataForm(forms.ModelForm):
    from_date = forms.DateField(label='From Date', widget=forms.DateInput(attrs={'type': 'date'}))
    to_date = forms.DateField(label='To Date', widget=forms.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = WeatherData
        fields = ['city', 'from_date', 'to_date']
