from django.urls import path
from weather import views


urlpatterns = [
    path('', views.WeatherDataView.as_view(), name='index'),
]
