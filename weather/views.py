from django.views import View
from django.shortcuts import render
from django.db.models import Q
from django.http import HttpRequest, HttpResponse

from .models import WeatherData
from .forms import WeatherDataForm
from .helpers import get_weather


class WeatherDataView(View):
    template_name: str = 'index.html'

    def get(self, request: HttpRequest) -> HttpResponse:
        form: WeatherDataForm = WeatherDataForm()
        return render(request, self.template_name, {'form': form, 'weather': None})

    def post(self, request: HttpRequest) -> HttpResponse:
        form: WeatherDataForm = WeatherDataForm(request.POST)

        if form.is_valid():
            city: str = form.cleaned_data['city']
            from_date = form.cleaned_data['from_date']
            to_date = form.cleaned_data['to_date']
            count_of_data = (to_date - from_date).days
            weather_data = WeatherData.objects.filter(Q(city=city) & Q(date__gte=from_date) & Q(date__lte=to_date))
            if not weather_data.exists() or len(weather_data) < count_of_data:
                weather_data = get_weather(city, from_date, to_date)
                for data in weather_data:
                    WeatherData.objects.create(
                        city=data.city, temperature=data.temperature, date=data.date, image=data.image
                    )
            return render(request, self.template_name, {'form': form, 'weather': weather_data})
        return render(request, self.template_name, {'form': form, 'error': 'All fields are required'})
