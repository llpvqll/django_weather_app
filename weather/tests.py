from django.test import TestCase
from django.urls import reverse
from .models import WeatherData


class WeatherDataViewTestCase(TestCase):
    def test_weather_data_post(self) -> None:
        test_city = 'Madrid'
        test_from_date = '2023-11-01'
        test_to_date = '2023-11-07'

        url = reverse('index')

        response = self.client.post(
            url,
            {
                'city': test_city,
                'from_date': test_from_date,
                'to_date': test_to_date,
            },
        )

        self.assertEqual(response.status_code, 200)

        # Check if the data was saved in the database
        weather_data = WeatherData.objects.filter(city=test_city, date__gte=test_from_date, date__lte=test_to_date)
        self.assertTrue(weather_data.exists())

    def test_empty_weather_form(self) -> None:
        url = reverse('index')
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'All fields are required')
