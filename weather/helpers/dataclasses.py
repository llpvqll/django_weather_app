from datetime import datetime

from pydantic import BaseModel


class WeatherDataClass(BaseModel):
    city: str
    temperature: float
    date: datetime
    image: str
