import os
import requests
from datetime import datetime
from .dataclasses import WeatherDataClass


WEATHER_API_KEY = os.getenv('WEATHER_API_KEY')
BASE_WEATHER_URL = 'http://api.weatherapi.com/v1/history.json'


def get_weather(location: str, from_date: datetime | None, to_date: datetime | None) -> list:
    weather = []
    params = {
        'key': WEATHER_API_KEY,
        'q': location,
        'dt': from_date,
        'end_dt': to_date,
    }
    response = requests.get(BASE_WEATHER_URL, params)  # type: ignore

    if response.status_code == 200:
        weather_data = response.json().get('forecast', {}).get('forecastday')
        for data in weather_data:
            weather.append(
                WeatherDataClass(
                    city=response.json().get('location', {}).get('name'),
                    temperature=data.get('day', {}).get('avgtemp_c'),
                    date=datetime.strptime(data.get('date'), "%Y-%m-%d"),
                    image=data.get('day', {}).get('condition', {}).get('icon'),
                )
            )
    return weather
