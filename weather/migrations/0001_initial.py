# Generated by Django 4.2.7 on 2023-11-13 14:53

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies: list = []

    operations = [
        migrations.CreateModel(
            name='WeatherData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.CharField(max_length=100)),
                ('temperature', models.FloatField()),
                ('date', models.DateField()),
                ('image', models.CharField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
